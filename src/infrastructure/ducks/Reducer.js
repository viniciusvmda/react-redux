import { combineReducers } from 'redux';
import SampleDuck, { DUCK_NAME as SAMPLE_DUCK } from './SampleDuck';
import CatalogoDuck, { DUCK_NAME as CATALOGO_DUCK } from './CatalogoDuck';

const reducers = {
    [SAMPLE_DUCK]: SampleDuck,
    [CATALOGO_DUCK]: CatalogoDuck,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;