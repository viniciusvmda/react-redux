export default [
    {
        id: 1,
        nome: "Celular",
        valor: 1500
    },
    {
        id: 2,
        nome: "Geladeira",
        valor: 2500
    },
    {
        id: 3,
        nome: "Notebook",
        valor: 3000
    },
    {
        id: 4,
        nome: "Cadeira Gamer",
        valor: 2000
    }
]