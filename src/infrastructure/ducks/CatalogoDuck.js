import MockItens from './MockItens';

export const DUCK_NAME = 'CatalogoDuck';

const INITIAL_STATE = () => ({
  itensCatalogo: [],
  carrinho: [],
  loading: false,
  error: false,
});

const ADICIONAR_ITEM_CARRINHO = `${DUCK_NAME}/ADICIONAR_ITEM_CARRINHO`;
const REMOVER_ITEM_CARRINHO = `${DUCK_NAME}/REMOVER_ITEM_CARRINHO`;
const INICIAR_CARREGAR_ITENS = `${DUCK_NAME}/INICIAR_CARREGAR_ITENS`;
const SUCESSO_CARREGAR_ITENS = `${DUCK_NAME}/SUCESSO_CARREGAR_ITENS`;
const FALHA_CARREGAR_ITENS = `${DUCK_NAME}/FALHA_CARREGAR_ITENS`;

export const adicionarItemCarrinho = (item) => ({ 
    type: ADICIONAR_ITEM_CARRINHO, item
});

export const removerItemCarrinho = (itemId) => ({ 
  type: REMOVER_ITEM_CARRINHO, itemId
});

const iniciarCarregarItens = () => ({ type: INICIAR_CARREGAR_ITENS });
const sucessoCarregarItens = (itensCatalogo) => ({ type: SUCESSO_CARREGAR_ITENS, itensCatalogo });
const falhaCarregarItens = () => ({ type: FALHA_CARREGAR_ITENS });

export const carregarItens = () => async (dispatch) => {
  dispatch(iniciarCarregarItens());
  try {
      const itens = [...MockItens];
      dispatch(sucessoCarregarItens(itens));
  } catch (err) {
    dispatch(falhaCarregarItens());
  }
};

const reducer = (state = INITIAL_STATE(), action) => {
  switch (action.type) {
    case ADICIONAR_ITEM_CARRINHO:
      return {
        ...state,
        carrinho: [...state.carrinho, action.item],
      };
      case REMOVER_ITEM_CARRINHO :
        const novoCarrinho = state.carrinho.filter((item) => item.id !== action.itemId);
        return {
          ...state,
          carrinho: [...novoCarrinho],
        }
      case INICIAR_CARREGAR_ITENS:
        return {
          ...state,
          loading: true,
          error: false,
        }
      case SUCESSO_CARREGAR_ITENS:
        return {
          ...state,
          loading: false,
          error: false,
          itensCatalogo: [...action.itensCatalogo]
        }
      case FALHA_CARREGAR_ITENS:
        return {
          ...state,
          loading: false,
          error: true,
        }
    default:
      return {
        ...state,
      };
  }
};

export default reducer;