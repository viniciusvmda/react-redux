export const DUCK_NAME = 'SampleDuck';

const INITIAL_STATE = () => ({
    stateValue: 0,
});

const SAMPLE_ACTION_TYPE = `${DUCK_NAME}/SAMPLE_ACTION`;

export const sampleAction = (newValue) => ({ 
    type: SAMPLE_ACTION_TYPE, 
    value: newValue
});

const reducer = (state = INITIAL_STATE(), action) => {
  switch (action.type) {
    case SAMPLE_ACTION_TYPE:
      return {
        ...state,
        stateValue: action.value
      };
    default:
      return {
        ...state,
      };
  }
};

export default reducer;