import React from 'react';
import './App.css';
import Catalogo from './pages/Catalogo';

function App() {
  return (
    <div className="App">
      <Catalogo/>
    </div>
  );
}

export default App;