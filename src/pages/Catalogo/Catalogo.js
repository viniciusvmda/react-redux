import React from 'react';
import ListagemCatalogo from '../../components/ListagemCatalogo';
import ResumoCarrinho from '../../components/ResumoCarrinho';
import './Catalogo.css';

const Catalogo = () => {
    return (
        <div className="catalogo">
            <header>
                <h1>Catálogo Virtual</h1>
            </header>
            <main>
                <ListagemCatalogo/>
                <ResumoCarrinho/>
            </main>
        </div>
    )
}

export default Catalogo;