import { connect } from 'react-redux';
import { removerItemCarrinho } from '../../infrastructure/ducks/CatalogoDuck';
import ResumoCarrinho from './ResumoCarrinho';

  const mapStateToProps = (state) => {
    const { CatalogoDuck } = state;
    return { carrinho: CatalogoDuck.carrinho };
  };
  
  const mapDispatchToProps = dispatch => ({
    removerItemCarrinho: (itemId) => {
      dispatch(removerItemCarrinho(itemId));
    },
  });
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(ResumoCarrinho);