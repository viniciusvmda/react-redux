import React from 'react';
import './ResumoCarrinho.css'
const ResumoCarrinho = ({ carrinho, removerItemCarrinho }) => {
    const renderizarListaItens = () => carrinho.map(item => 
        <div key={item.id} className="catalogo-resumo-item">
            <button onClick={() => removerItemCarrinho(item.id)}>x</button>
            <span className="catalogo-resumo-itemName">{item.nome}</span>
        </div>
    );

    return (
        <section className="catalogo-carrinho">
            <div>Carrinho: {carrinho.length} itens</div>
            {renderizarListaItens()}
        </section>
    )
}

export default ResumoCarrinho;