import { connect } from 'react-redux';
import ItemCatalogo from './ItemCatalogo';

import { adicionarItemCarrinho } from '../../infrastructure/ducks/CatalogoDuck';
  
const mapStateToProps = (state) => {
  const { CatalogoDuck } = state;
  return { carrinho: CatalogoDuck.carrinho };
};

const mapDispatchToProps = dispatch => ({
    adicionarItemCarrinho: (item) => {
        dispatch(adicionarItemCarrinho(item));
    },
});
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ItemCatalogo);