import React from 'react';
import './ItemCatalogo.css';

const ItemCatalogo = ({ item, adicionarItemCarrinho, carrinho }) => {
    const itemJaCadastrado = carrinho.find(itemCarrinho => itemCarrinho.id === item.id) !== undefined;
    
    return (
        <article className="catalogo-item">
            <div>{item.nome}</div>
            <div>Valor: R${item.valor}</div>
            {!itemJaCadastrado ? 
                <button type="button" onClick={() => adicionarItemCarrinho(item)}>+</button>
            : null}
        </article>
    )
}

export default ItemCatalogo;