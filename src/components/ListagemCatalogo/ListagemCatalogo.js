import React, { useEffect } from 'react';
import PropTypes from 'prop-types'
import ItemCatalogo from '../ItemCatalogo';
import './ListagemCatalogo.css';

const ListagemCatalogo = ({ itens, carregarItens }) => {
    useEffect(() => {
        carregarItens();
    }, [carregarItens]);
    
    return (
        <section className="catalogo-listagem">
            {itens ? itens.map(item => 
               <ItemCatalogo key={item.id} item={item}/>
            ) : null}
        </section>
    )
}

ListagemCatalogo.propTypes = {
    itens: PropTypes.array,
    carregarItens: PropTypes.func.isRequired
}

ListagemCatalogo.defaultProps = {
    itens: [],
}

export default ListagemCatalogo;