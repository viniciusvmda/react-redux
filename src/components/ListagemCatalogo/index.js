import { connect } from 'react-redux';
import ListagemCatalogo from './ListagemCatalogo';

import { carregarItens } from '../../infrastructure/ducks/CatalogoDuck';
  
  const mapStateToProps = (state) => {
    const { CatalogoDuck } = state;
    return { itens: CatalogoDuck.itensCatalogo };
  };
  
  const mapDispatchToProps = dispatch => ({
    carregarItens: () => {
      dispatch(carregarItens());
    },
  });
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ListagemCatalogo);