# Configuração
* É possível criar o projeto utilizando um template oficial contendo React + Redux

```npx create-react-app my-app --template redux```
* Também é possível adicionar o redux em uma aplicação existente
* É possível instalar @reduxjs/toolkit que já vem com alguns pacotes instalados 

```yarn add @reduxjs/toolkit```

ou instalar o core do Redux e adicionar os pacotes manualmente

```yarn add redux```

## Pacotes adicionais
### react-redux
* Faz a conexão do React com o Redux

```yarn add react-redux```

### redux-thunk
* Middleware para tratar chamadas assíncronas.
* Permite adicionar uma função dentro da action ao invés de um objeto.
* Chamará o dispatch uma vez no componente e ao menos uma vez dentro da função criada.
* Está incluído no redux toolkit

```yarn add redux-thunk```

### redux-persist
* Persite o estado do Redux. Assim, ao fechar a página o estado não será limpo.

```yarn add redux-persist```

### redux-devtools-extension
* Permite utiliza a ferramenta de debug do Redux
* Pode ser habilitada/desabilitada em produção como é mostrado [aqui](https://medium.com/@zalmoxis/using-redux-devtools-in-production-4c5b56c5600f)

```yarn add redux-devtools-extension```

# Execução
Para executar a aplicação executar o seguinte comando

```yarn start```